#GENERADORES

* rails new [nombre_proyecto] ==> Genera nuevo proyecto
* rails generate controller [nombre_controller] [controller1] [controller2] [controllerN] ==> Genera Controllers
* rails routes ==> Muestra las rutas de los controllers
* http://localhost:3000/rails/info/routes ==> Muestra las rutas

# TAGS HTML

* <%= %> Sirve para mostrar contenido
* <% %> Sirve para evaluar el contenido
* <%= yield %> placeholder o contenedor

#METODOS

* <%= link_to 'nombre_link', pages_xxx_path %> ==> Permite adicionar Links
* <%= image_tag 'nombre_imagen' %> ==>Permite ingresar imagenes (deben encontrarse dentro de Asset/images)

#HEROKU

* heroku login
* heroku create
* bundle
* git push heroku master
* heroku open

# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
